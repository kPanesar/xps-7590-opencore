# Dell XPS 7590 OpenCore

Using **OpenCore 0.6.6**

- **Laptop**: Dell XPS 7590 (2019) 4K IPS with Nvidia GeForce GTX 1650
- **CPU**: Intel i7-9750H
- **GPU**: Intel UHD Graphics 630 (GTX 1650 disabled)
- **RAM**: 16GB 2667 MHz DDR4
- **SSD**: SanDisk Ultra M.2 NVMe 1TB (Don't use SK Hynix NVMe)
- **Wifi/BT Card**: Killer Wi-Fi 6 AX1650
- **BIOS**: 1.9.1
- **What works**: Sleep, display brightness, battery indicator, external display with HDMI (4K), WiFi, Bluetooth, touchpad with multi-touch gestures, keyboard media keys, audio
- **What doesn't work**: Touchscreen, DRM content, keyboard shortcut keys for adjusting display brightness, and fingerprint sensor
- **Not sure if it works**: Thunderbolt

## Setup
- Rename `sample-config.plist` to `config.plist`
- Add SMBIOS information to `config.plist`
- Add the `Resources` folder from [here](https://github.com/acidanthera/OcBinaryData) to the `EFI/OC` directory
- Follow the official guide for setup/installation: https://dortania.github.io/OpenCore-Install-Guide/